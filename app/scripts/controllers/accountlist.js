'use strict';

/**
 * @ngdoc function
 * @name sbDemoApp.controller:AccountlistCtrl
 * @description
 * # AccountlistCtrl
 * Controller of the sbDemoApp
 */
angular.module('sbDemoApp')
  
  .controller('AccountlistCtrl', function ($scope,$rootScope,$state,userdata,configUrl) {

  	var url,dataToSend;
     dataToSend = {
    "systemPrincipalIdentifier": {
        "systemPrincipalId":$rootScope.systemPrincipalId
    }
}

    
console.log(dataToSend);

    url = configUrl.url.baseUrl + configUrl.url.accountList;
      $scope.data = {};
      $scope.goToConf = function() {
      };
    userdata.getRemoteData(url,dataToSend, function(result) {
        $scope.data = result;
        console.log(result);
      });
  });
