'use strict';

/**
 * @ngdoc function
 * @name sbDemoApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the sbDemoApp
 */
angular.module('sbDemoApp')
  .controller('DashboardCtrl', function ($scope,$state) {
   
    $scope.GoToAllAccount = function (){
    	$state.go('AppMainView.accountlist');
    };
   
    $scope.GoToBiller = function (){
    	$state.go('AppMainView.allbiller');
    };
  });
