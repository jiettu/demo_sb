'use strict';

/**
 * @ngdoc function
 * @name sbDemoApp.controller:MainviewCtrl
 * @description
 * # MainviewCtrl
 * Controller of the sbDemoApp
 */
angular.module('sbDemoApp')
  .controller('MainviewCtrl', function ($scope,$state,$rootScope) {
    
        /*Added for E2E Test Only for ui-router Going to Parent View On Page reload, $state.current.name === '' added for unit test*/
      if($state.current.name === 'AppMainView' || $state.current.name === 'AppMainView.dashboard' || $state.current.name === '')  
        $state.transitionTo('AppMainView.dashboard');
      else
        $state.transitionTo($state.current.name);
      $scope.data = $rootScope.dashboardData;
     
      $scope.showOverlay = false;
      
      $scope.goToDashboard = function(){
        $state.go('AppMainView.dashboard');
      };
      $scope.megaMenuClick = function (){
        if($scope.showOverlay){
          $scope.showOverlay = false;
        }
        else{
          $scope.showOverlay = true;
        }
      };
   
  });
