'use strict';

/**
 * @ngdoc function
 * @name sbDemoApp.controller:LogincontrollerCtrl
 * @description
 * # LogincontrollerCtrl
 * Controller of the sbDemoApp
 */
angular.module('sbDemoApp')
  .controller('LogincontrollerCtrl', function ($scope,$rootScope,$state,$window,userservice,authservice,configUrl) {
    /*To Be Removed*/
     authservice.isLogged = false;
                delete $window.sessionStorage.token;
    $scope.errorMsgIsVisible = false;
    $scope.errorMsg = 'Hello';
     $scope.User = {};
    $scope.User.UserId ='';
    $scope.User.Password ='';

    $scope.logIn = function logIn(username, password) {
                //To be remove
console.log(username+' '+password);
                
                authservice.isLogged = false;
                delete $window.sessionStorage.token;
                if(username != '' && password != ''){
                                var headerResponce,url;
                                var digitalId = {
                                            'digitalId': {
                                                'devices': [
                                                  {
                                                    'deviceType': 'TABLET',
                                                    'name': 'My Android (samsung Nexus 10)',
                                                    'operatingSystem': 'ANDROID',
                                                    'uniqueIndentifier': '52B074854ABAB6222465B4C85EDB3BC4'
                                                  }
                                                ],
                                                'password': '',
                                                'username': ''
                                              }
                                              };
                
                                url = configUrl.url.baseUrl + configUrl.url.loginScreen; //'../json/AuthenticatDI_Response.json';//sbDemoApp.config.basePath + sbDemoApp.config.basePath.login
                                digitalId.digitalId.password = password;
                                digitalId.digitalId.username = username;
                                 userservice.logIn(url,digitalId).success(function(data,status, headers) {
                                   /*Seetind data to be send during service calls*/
                                    $rootScope.systemPrincipalKey = data.userProfile.channelProfiles[1].systemPrincipalIdentifiers[0].systemPrincipalKey;
                                    $rootScope.systemPrincipalId = data.userProfile.channelProfiles[1].systemPrincipalIdentifiers[0].systemPrincipalId;
                                
                                    authservice.isLogged = true;
                                    headerResponce = headers();
                                    //XXXXX need to removed during integration
                                    $window.sessionStorage.token = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';//headerResponce['x-sbg-token'];
                                    $rootScope.dashboardData = data;
                                     $state.go('AppMainView');
                
                                }).error(function(status, data) {
                                  console.log(status);
                                    console.log(data);
                                });}
                else
                {
                  if(username === '' && password === '')
                    $scope.errorMsg = 'Please Enter Your Username and Password';
                   else if(username === '')
                      $scope.errorMsg = 'Please Enter Your Username';
                    else
                      $scope.errorMsg = 'Please Enter Your Password';
                    $scope.errorMsgIsVisible = true;

                }
            };
    
        $scope.logout = function logout() {
            if (authservice.isLogged) {
                authservice.isLogged = false;
                delete $window.sessionStorage.token;
                //$location.path('/');
            }
        };
  });
