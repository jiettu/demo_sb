'use strict';

/**
 * @ngdoc function
 * @name sbDemoApp.controller:BillerlistCtrl
 * @description
 * # BillerlistCtrl
 * Controller of the sbDemoApp
 */
angular.module('sbDemoApp')
  .controller('BillerlistCtrl', function ($scope,$rootScope,$state,userdata,configUrl) {
    var url,dataToSend;
       url = configUrl.url.baseUrl + configUrl.url.billerList;
      $scope.data = {};
      dataToSend = {
    "systemPrincipalIdentifier": {
        "systemPrincipalKey": $rootScope.systemPrincipalKey,
        "systemPrincipalId":$rootScope.systemPrincipalId
    }
}

console.log(dataToSend);

    userdata.getRemoteData(url,dataToSend, function(result) {
        $scope.data = result;
        console.log(result);
      });
  });
