'use strict';

/**
 * @ngdoc function
 * @name sbDemoApp.controller:AppcontrollerCtrl
 * @description
 * # AppcontrollerCtrl
 * Controller of the sbDemoApp
 */
angular.module('sbDemoApp')
  .controller('AppcontrollerCtrl', function ($scope) {
    $scope.dashboardData = {};
    $scope.systemPrincipalKey = '';
    $scope.systemPrincipalId = '';
  });
