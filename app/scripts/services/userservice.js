'use strict';

/**
 * @ngdoc service
 * @name sbDemoApp.userservice
 * @description
 * # userservice
 * Factory in the sbDemoApp.
 */
angular.module('sbDemoApp')
  .factory('userservice', function ($http) {
     
     return {
        logIn: function(url,data) {
            return $http.post(url, data); //Post is not working with dev server
            //return $http.get(url);
            
        },
 
        logOut: function() {
 
        }
    };
  });
