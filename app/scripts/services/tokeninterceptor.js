'use strict';

/**
 * @ngdoc service
 * @name sbDemoApp.Tokeninterceptor
 * @description
 * # Tokeninterceptor
 * Factory in the sbDemoApp.
 */
angular.module('sbDemoApp')
  .factory('Tokeninterceptor', function ($q,$window) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers['x-sbg-token'] = $window.sessionStorage.token;
            }
            return config;
        },
 
        response: function (response) {
            return response || $q.when(response);
        }
    };
  });