'use strict';

/**
 * @ngdoc service
 * @name sbDemoApp.userdata
 * @description
 * # userdata
 * Factory in the sbDemoApp.
 */
angular.module('sbDemoApp')
  .factory('userdata', function ($http,$window) {
    
    return {
      getRemoteData: function(url,data, callback) {
        console.log($window.sessionStorage.token);
          return $http.post(url, data)
              .success(callback);
      }
    };
  });
