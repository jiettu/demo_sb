'use strict';

/**
 * @ngdoc service
 * @name sbDemoApp.authservice
 * @description
 * # authservice
 * Factory in the sbDemoApp.
 */
angular.module('sbDemoApp')
  .factory('authservice', function () {
  var auth = {
        isLogged: false
    };
 
    return auth;
  });
