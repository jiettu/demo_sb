'use strict';

/**
 * @ngdoc directive
 * @name sbDemoApp.directive:listdisplay
 * @description
 * # listdisplay
 */
angular.module('sbDemoApp')
  .directive('listDisplay', function () {
    return {

      restrict: 'A',
      scope : {
      	account : '=accountdata'
      },
      templateUrl : 'views/template/cardview.html',
      // link: function postLink(scope, element, attrs) {
        
      // }
    };
  });
