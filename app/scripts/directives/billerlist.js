'use strict';

/**
 * @ngdoc directive
 * @name sbDemoApp.directive:billerlist
 * @description
 * # billerlist
 */
angular.module('sbDemoApp')
  .directive('billerlistDisplay', function () {
    return {
      templateUrl: 'views/template/cardviewbiller.html',
      restrict: 'EA',
      scope : {
      	billdisplay : '=billerdata'
      }
    };
  });
