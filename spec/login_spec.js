describe('Login View ', function() {

	/*New Approch Not undated in other test*/
	var urlLogin;
	var userid = element(by.model('User.UserId'));
  	var password =  element(by.model('User.Password'));
  	var loginButton = element(by.css('.ben-next-text'));
  	var errorMessage = element(by.css('.errMsg'));
    
    beforeEach(function() {
      browser.get('dist/index.html#/login');
    });


    it('should render Login View when user navigates to /login', function() {
      urlLogin = browser.getCurrentUrl();
      expect(element(by.css('.enter-pwd-text')).getText()).
        toMatch(/Enter User Name/);
    });
  
  	it('Should show Error Message If ID and Password Feild Is Empty ', function() {
	    userid.clear();
	    password.clear();
	    loginButton.click();
	    expect(errorMessage.getText()).toMatch('Please Enter Your Username and Password');

	    
  });
  	it('Should show Error Message If userID Feild Is Empty ', function() {
	    userid.clear();
	    password.clear();

	    password.sendKeys('Foo');
	    loginButton.click();
	    expect(errorMessage.getText()).toMatch('Please Enter Your Username');

  });
  	it('Should show Error Message If Password Feild Is Empty ', function() {
	    userid.clear();
	    password.clear();
	    
	    userid.sendKeys('Foo');
	    loginButton.click();
	    expect(errorMessage.getText()).toMatch('Please Enter Your Password');
  });

  		it('Should Navigate to dashboard when Feilds are not Empty ', function() {
			userid.clear();
   		  	password.clear();

		    userid.sendKeys('demo@demo.com');
		    password.sendKeys('Bhaskar');
		    loginButton.click();
		    expect(browser.getCurrentUrl()).not.toEqual(urlLogin);
  });

  });