
describe('E2E Routing Test', function(){

browser.get('dist/index.html#/appmainview');

var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};

/*it('Login To Dashboard', function() {
      element(by.model('User.UserId')).sendKeys('abc@mail.com');
      element(by.model('User.Password')).sendKeys('BHASKAR');
      element(by.css('.ben-next-text')).click();
      expect(browser.getLocationAbsUrl()).toMatch("/appmainview");

    });*/

it('Dashboard To Account List and back To Dashboard', function() {
  element(by.css('.goToAcc')).click();
  expect(browser.getLocationAbsUrl()).toMatch("/AccountList");
  element(by.css('.allaccounts-sub-header-image')).click();
  expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
      
    });
it('Dashboard To Account List and back To Dashboard', function() {
  element(by.css('.goToBiller')).click();
  expect(browser.getLocationAbsUrl()).toMatch("/AllBiller");
  element(by.css('.allaccounts-sub-header-image')).click();
  expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
      
    })

it('Dashboar Overlay Should Open/ Close On Cross click', function(){
  expect(hasClass(element(by.css('.menu-overlay')), 'ng-hide')).toBe(true);
  element(by.css('.logout-pic')).click();

  expect(hasClass(element(by.css('.menu-overlay')), 'ng-hide')).toBe(false);
  element(by.css('.logout-pic')).click();

  expect(hasClass(element(by.css('.menu-overlay')), 'ng-hide')).toBe(true);
});
});