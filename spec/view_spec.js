describe("E2E: Testing Views", function() {

  beforeEach(function() {
    browser.get('dist/');
  });

 
   it('should automatically redirect to /loginscreen when location hash/fragment is empty', function() {
    browser.get('dist/#');
    expect(browser.getLocationAbsUrl()).toMatch('/login');
  });

    it('should have a working /mainview route and it should load dashboard view', function() {
    browser.get('dist/#/appmainview')
    expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
  });

      it('should have a working /AccountList route ', function() {
    browser.get('dist/#/appmainview/AccountList');
    expect(browser.getLocationAbsUrl()).toMatch('/AccountList');
  });

       it('should have a working /AccountList route ', function() {
    browser.get('dist/#/appmainview/AllBiller')
    expect(browser.getLocationAbsUrl()).toMatch('/AllBiller');
  });

});