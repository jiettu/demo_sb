'use strict';

describe('Directive: listdisplay', function () {

  // load the directive's module
  beforeEach(module('sbDemoApp'));

  //Load template
  beforeEach(module('foo'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<div list-display accountdata="account"></div>');
    element = $compile(element)(scope);
    scope.$digest();
    expect(element.find("p").length).toEqual(6);
  }));
});
