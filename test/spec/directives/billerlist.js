'use strict';

describe('Directive: billerlist', function () {

  // load the directive's module
  beforeEach(module('sbDemoApp'));
  beforeEach(module('foo'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should give number of p tags', inject(function ($compile) {
    element = angular.element('<div billerlist-display billerdata="biller"></div>');
    element = $compile(element)(scope);
    scope.$digest();
    expect(element.find("p").length).toEqual(5);
  }));
});
