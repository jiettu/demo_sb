'use strict';

describe('Service: Tokeninterceptor', function () {

  // load the service's module
  beforeEach(module('sbDemoApp'));

  // instantiate service
  var Tokeninterceptor;
  beforeEach(inject(function (_Tokeninterceptor_) {
    Tokeninterceptor = _Tokeninterceptor_;
  }));

  it('should do something', function () {
    expect(!!Tokeninterceptor).toBe(true);
  });

});
