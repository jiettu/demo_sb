'use strict';

describe('Controller: MainviewCtrl', function () {

  // load the controller's module
  beforeEach(module('sbDemoApp'));

  var HeaderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HeaderCtrl = $controller('MainviewCtrl', {
      $scope: scope
    });
  }));

  it('Should toggle Overlay', function () {
    expect(scope.showOverlay).toBe(false);
    scope.megaMenuClick();
    expect(scope.showOverlay).toBe(true);
    scope.megaMenuClick();
    expect(scope.showOverlay).toBe(false);
  });
});
